/***************************************************************************
                          random.h  -  description
                             -------------------
    begin                : Tue Dec 12 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RANDOM_H
#define RANDOM_H

#include "object.h"
#include <stdlib.h>

class Random: public Object{
  public:
    Random():Object(){};
    ~Random(){};

    int getRandomNumber(int low, int high);
};

#endif

