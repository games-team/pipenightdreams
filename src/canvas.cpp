/***************************************************************************
                          canvas.cpp  -  description
                             -------------------
    begin                : Mon Oct 16 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "canvas.h"

Canvas::Canvas():Graphic(){
  x=y=0;
}

Canvas::Canvas(int width, int height):Graphic(width, height){
  x=y=0;
}

void Canvas::setPos(int x, int y){
  this->x=x;this->y=y;
}

int Canvas::getX(){
  return x;
}

int Canvas::getY(){
  return y;
}


