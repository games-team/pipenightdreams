/***************************************************************************
                          elbowupleft.cpp  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "elbowupleft.h"
#include "pointerelbowupleft.h"

ElbowUpLeft::ElbowUpLeft():Pipe(){
  p=new PointerElbowUpLeft();
}

ElbowUpLeft::~ElbowUpLeft(){
  delete p;
}

bool ElbowUpLeft::hasConnection(CardinalPoint con){
  return ((con==North) || (con==West));
}

CardinalPoint ElbowUpLeft::getOutput(CardinalPoint input){
  if (input==ro) return Void;
  switch (input){
    case North: return West;
    case West:return North;
    default:return Void;
  }
}

void ElbowUpLeft::restrictAsOutput(CardinalPoint con){
  if (con==North || con==West) ro=con;
}

bool ElbowUpLeft::isRestrictedAsOutput(CardinalPoint con){
  return (con==ro);
}

void ElbowUpLeft::incFullLevel(CardinalPoint input,unsigned int amount){
  if ((input==North || input==West) && ro!=input)
    if ((used_input==Void) || (used_input==input)){
      full_level+=amount;
      used_input=input;
    }
}

int ElbowUpLeft::getFullLevel(CardinalPoint input){
  if (input==used_input)
    return full_level;
  return -1;
}

Pointer * ElbowUpLeft::getPointer(){
  return p;
}

void ElbowUpLeft::paint(VideoManager * vm){
  Image * ima;
  if (fixed)
    ima=(vm->getImageManager())->getImage(new Str("elbow_nwb.png"));
  else
    ima=(vm->getImageManager())->getImage(new Str("elbow_nw.png"));
  vm->blit(ima, x, y);

  if (ro!=Void) paintRestriction(vm, ro);

  if (full_level>0){
    Image * aux=(vm->getImageManager())->getImage(new Str("flow.png"));
    float fwidth=(float)PipeWidth/2;
    float fheight=(float)PipeHeight/2;
    float nov=PI/2;
    vm->setClipping(x, y, PipeWidth, PipeHeight);
    vm->enableClipping(true);
    int xaux=x-aux->width()/2;
    int yaux=y-aux->width()/2;
    if (used_input==North){
      for (int i=0; i<full_level+1;i+=Gran)
        vm->blit(aux, (int)(xaux+(float)cos((float)i/(float)full() * nov)*fwidth),
                      (int)(yaux+(float)sin((float)i/(float)full() * nov)*fheight));
    }else{
      for (int i=0; i<full_level+1;i+=Gran)
        vm->blit(aux, (int)(xaux+(float)cos((float)(full()-i)/(float)full() * nov)*fwidth),
                      (int)(yaux+(float)sin((float)(full()-i)/(float)full() * nov)*fheight));
    }
    vm->enableClipping(false);
  }
  if (bonus!=NormalBonus) paintBonus(vm, bonus);
}
