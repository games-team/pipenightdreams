/***************************************************************************
                          canvas.h  -  description
                             -------------------
    begin                : Fri Sep 1 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CANVAS_H
#define CANVAS_H

#include "graphic.h"
#include "videomanager.h"

/** Clase abstracta para el manejo de objetos gr�ficos.*/

class Canvas: public Graphic{

  public:

    /** Constructor.*/
    Canvas();

    Canvas(int width, int height);

    /** Destructor.*/
    virtual ~Canvas(){};

    /** Setea la posici�n.*/
    virtual void setPos(int x, int y);

    virtual int getX();

    virtual int getY();

    /** Pinta el canvas.*/
    virtual void paint(VideoManager * vm)=0;

  protected:

    int x, y;
};

#endif


