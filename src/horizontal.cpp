/***************************************************************************
                          horizontal.cpp  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "horizontal.h"
#include "pointerhorizontal.h"

Horizontal::Horizontal():Pipe(){
  p=new PointerHorizontal();
}

Horizontal::~Horizontal(){
  delete p;
}

bool Horizontal::hasConnection(CardinalPoint con){
  return ((con==West) || (con==East));
}

CardinalPoint Horizontal::getOutput(CardinalPoint input){
  if (input==ro) return Void;
  switch (input){
    case West: return East;
    case East:return West;
    default:return Void;
  }
}

void Horizontal::restrictAsOutput(CardinalPoint con){
  if (con==East || con==West) ro=con;
}

bool Horizontal::isRestrictedAsOutput(CardinalPoint con){
  return (con==ro);
}

void Horizontal::incFullLevel(CardinalPoint input,unsigned int amount){
  if (input==West || input==East)
    if ((used_input==Void || used_input==input) && input!=ro){
      full_level+=amount;
      used_input=input;
    }
}

int Horizontal::getFullLevel(CardinalPoint input){
  if (input==used_input)
    return full_level;
  return 0;
}

Pointer * Horizontal::getPointer(){
  return p;
}

void Horizontal::paint(VideoManager * vm){
  Image * ima;
  if (fixed)
    ima=(vm->getImageManager())->getImage(new Str("horizontal_b.png"));
  else
    ima=(vm->getImageManager())->getImage(new Str("horizontal.png"));
  vm->blit(ima, x, y);

  if (ro!=Void) paintRestriction(vm, ro);

  paintFlow(vm);
}

void Horizontal::paintFlow(VideoManager * vm){

  if (full_level>0){
    Image * aux=(vm->getImageManager())->getImage(new Str("flow.png"));
    int yof=y+(PipeHeight-aux->height())/2;
    int total,f;
    vm->setClipping(x, y, PipeWidth, PipeHeight);
    vm->enableClipping(true);
    if (used_input==West){
      total=x-aux->width()/2;
      f=1;
    }
    else{
      total=x+PipeWidth-aux->width()/2;
      f=-1;
    }
    for (int i=0; i<full_level+2*Gran;i+=Gran)
      vm->blit(aux, (int)(total+f*(float)i/(float)full()*(float)PipeWidth), yof);
    vm->enableClipping(false);
  }
  if (bonus!=NormalBonus) paintBonus(vm, bonus);
}

