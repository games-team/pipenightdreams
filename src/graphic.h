/***************************************************************************
                          graphic.h  -  description
                             -------------------
    begin                : Sat Oct 14 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GRAPHIC_H
#define GRAPHIC_H

#include "surface.h"
#include "SDL.h"
#include <string.h> // Only for Red Hat


#define OPAQUE		255
#define TRANSPARENT	0


enum Axis {HAxis, VAxis};

class Graphic: public Surface{

  public:

    Graphic();

    Graphic(Graphic * g);

    Graphic(int width, int height);

    virtual ~Graphic();

    void setAlpha(char value=OPAQUE);

    /** Enable or disable clipping.*/
    void enableClipping(bool flag=true);

    /** Establish the cliping zone.*/
    void setClipping(int top, int left, int bottom, int right);

    virtual void flip(Axis a=HAxis);

    virtual void fill(int r, int g, int b);

    virtual void fillRect(int x, int y, int w, int h, int r, int g, int b, int a);

  protected:

    int clip_x, clip_y, clip_width, clip_height;
    bool clip_active;
    void * pixels;
};


#endif

