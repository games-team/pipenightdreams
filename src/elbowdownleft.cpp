/***************************************************************************
                          elbowdownleft.cpp  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "elbowdownleft.h"
#include "pointerelbowdownleft.h"

ElbowDownLeft::ElbowDownLeft():Pipe(){
  p=new PointerElbowDownLeft();
}

ElbowDownLeft::~ElbowDownLeft(){
  delete p;
}

bool ElbowDownLeft::hasConnection(CardinalPoint con){
  return ((con==West) || (con==South));
}

CardinalPoint ElbowDownLeft::getOutput(CardinalPoint input){
  if (input==ro) return Void;
  switch (input){
    case South: return West;
    case West:return South;
    default:return Void;
  }
}

void ElbowDownLeft::restrictAsOutput(CardinalPoint con){
  if (con==South || con==West)  ro=con;
}

bool ElbowDownLeft::isRestrictedAsOutput(CardinalPoint con){
  return (ro==con);
}

void ElbowDownLeft::incFullLevel(CardinalPoint input,unsigned int amount){
  if ((input==South || input==West) && ro!=input)
    if ((used_input==Void) || (used_input==input)){
      full_level+=amount;
      used_input=input;
    }
}

int ElbowDownLeft::getFullLevel(CardinalPoint input){
  if (input==used_input) return full_level;
  return 0;
}

Pointer * ElbowDownLeft::getPointer(){
  return p;
}

void ElbowDownLeft::paint(VideoManager * vm){
  Image * ima;
  if (fixed)
    ima=(vm->getImageManager())->getImage(new Str("elbow_swb.png"));
  else
    ima=(vm->getImageManager())->getImage(new Str("elbow_sw.png"));
  vm->blit(ima, x, y);

  if (ro!=Void) paintRestriction(vm,ro);
  if (full_level>0){
    Image * aux=(vm->getImageManager())->getImage(new Str("flow.png"));
    float fwidth=(float)PipeWidth/2;
    float fheight=(float)PipeHeight/2;
    float nov=PI/2;
    vm->setClipping(x, y, PipeWidth, PipeHeight);
    vm->enableClipping(true);
    int xaux=x-aux->width()/2;
    int yaux=y+PipeHeight-aux->width()/2;
    if (used_input==South){
      for (int i=0; i<full_level+1;i+=Gran)
        vm->blit(aux, (int)(xaux+(float)cos((float)i/(float)full() * nov)*fwidth),
                      (int)(yaux-(float)sin((float)i/(float)full() * nov)*fheight));
    }else{
      for (int i=0; i<full_level+1;i+=Gran)
        vm->blit(aux, (int)(xaux+(float)cos((float)(full()-i)/(float)full() * nov)*fwidth),
                      (int)(yaux-(float)sin((float)(full()-i)/(float)full() * nov)*fheight));
    }
    vm->enableClipping(false);
  }
  if (bonus!=NormalBonus) paintBonus(vm, bonus);
}
