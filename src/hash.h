/***************************************************************************
                          hash.h  -  description
                             -------------------
    begin                : Tue Aug 22 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef HASH_H
#define HASH_H

#include "object.h"
#include "str.h"
#include "list.h"

/** Implementación de tabla hash. */

class Hash: public Object{

  public:

    enum Result {AddedNoCollision, AddedCollision, AddedOverwrite,
                 Removed, NotFound, NotAdded};

    /** Constructor default.
    @param bs Cantidad de buckets a reservar.
    */
    Hash(int bs=256);

    /** Destructor. */
    virtual ~Hash();

    /** Inserta un objeto asociado a una cadena en la tabla.
    @param str La cadena en cuestión, no debe utilizarse ni liberarse,
               luego de usarla como parámetro a esta funcion.
    @param obj El objeto en cuestion.
    @return AddedNoCollision, AddedCollision, o AddedOverwrite, NotAdded.
    */
    Result add(Str * str, Object * obj);

    /** Busca el objeto asociado a la cadena dada. Retorna NULL
    si no lo encuentra.*/
    Object * find(Str * str);

    /** Quita de la tabla el objeto asociado a la cadena str.
        @param del Si es true el objeto es eliminado.
        @return Removed o NotFound.
    */
    Result remove(Str * str, bool del=false);

    /** Quita todos los objetos de la tabla.
    @param del Si es true los elimina.
    */
    void empty(bool del=true);

  protected:

    int nbuckets;
    List ** lbuckets;

    virtual int function(Str * str);

    class Bucket: public Object{

      public:

        Bucket(Str * str, Object * obj);
        ~Bucket();

        void setStr(Str * str);
        void setObject(Object * obj);

        Str * getStr();
        Object * getObject();

      protected:
        Str * str;
        Object * obj;
    };
};
#endif


