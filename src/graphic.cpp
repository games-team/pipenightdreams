/***************************************************************************
                          graphic.cpp  -  description
                             -------------------
    begin                : Sat Oct 14 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "graphic.h"
#include <stdlib.h>

Graphic::Graphic():Surface(){
  pixels=NULL;
  clip_active=false;
};

Graphic::Graphic(int width, int height)
  :Surface(width, height){
  pixels=NULL;
  clip_active=false;
}

Graphic::Graphic(Graphic * g){
  pixels= malloc(g->surface->w*g->surface->h*g->surface->format->BytesPerPixel);
  memcpy(pixels, g->surface->pixels,g->surface->w*g->surface->h*g->surface->format->BytesPerPixel);

  surface=SDL_CreateRGBSurfaceFrom(pixels,
    g->surface->w, g->surface->h, g->surface->format->BitsPerPixel,
    g->surface->pitch, g->surface->format->Rmask, g->surface->format->Gmask,
    g->surface->format->Bmask, g->surface->format->Amask);
  clip_active=false;
}

Graphic::~Graphic(){
  if (pixels) free(pixels);
}

void Graphic::setAlpha(char value=OPAQUE){
  SDL_SetAlpha(surface, SDL_SRCALPHA, value);
}

void Graphic::enableClipping(bool flag=true){

  if (flag){
    SDL_Rect rect;
    rect.x=clip_x;
    rect.y=clip_y;
    rect.w=clip_width;
    rect.h=clip_height;
    SDL_SetClipRect(surface, &rect);
  }else
    SDL_SetClipRect(surface, NULL);
}

void Graphic::setClipping(int x, int y, int width, int height){
  clip_y=y;
  clip_x=x;
  clip_width=width;
  clip_height=height;
}

void Graphic::flip(Axis a=HAxis){
  if (surface){
    if (SDL_MUSTLOCK(surface))
      if (SDL_LockSurface(surface)<0) return;
    int i,j,width=surface->w, height=surface->h, aw,bw;
    Uint32 * p, *q, aux, *ap;
    p=(Uint32*)(surface->pixels);
    if (a==HAxis){
      q=p+width*height-1;
      height/=2;
      for (j=0;j<height;j++){
        for (i=0;i<width;i++){
            aux=*p;
            *p++=*q;
            *q--=aux;
        }
      }
    }
    else{
      aw=width>>1;
      bw=width-1;
      for (j=0;j<height;j++){
        ap=p;
        q=p+bw;
        for (i=0;i<aw;i++){
            aux=*p;
            *p++=*q;
            *q--=aux;
        }
        p=ap+width;
      }
    }
    if (SDL_MUSTLOCK(surface))
      SDL_UnlockSurface(surface);
  }
}

void Graphic::fill(int r, int g, int b){
  SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, r, g, b));
}

void Graphic::fillRect(int x, int y, int w, int h, int r, int g, int b, int a){
  SDL_Rect rect;

  rect.x=x;
  rect.y=y;
  rect.w=w;
  rect.h=h;

  SDL_FillRect(surface, &rect, SDL_MapRGBA(surface->format, r, g, b, a));
}

