/***************************************************************************
                          surface.h  -  description
                             -------------------
    begin                : Sat Oct 14 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SURFACE_H
#define SURFACE_H

#include "object.h"
#include "SDL.h"

class VideoManager;

class Surface: public Object{

  public:

    Surface();

    /** Constructor.*/
    Surface(int width, int height);

    /** Destructor.*/
    virtual ~Surface();

    /**
    @return El ancho de la surface.
    */
    virtual int width();

    /**
    @return El alto de la surface.
    */
    virtual int height();

  protected:

    friend class VideoManager;
    friend class Sprite;
    SDL_Surface * surface;

};
#endif

