/***************************************************************************
                          eventmanager.cpp  -  description
                             -------------------
    begin                : Fri Sep 15 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "eventmanager.h"

EventManager::EventManager(){

  SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);
  SDL_EventState(SDL_MOUSEBUTTONDOWN, SDL_IGNORE);
  SDL_EventState(SDL_MOUSEBUTTONUP, SDL_IGNORE);
  SDL_EventState(SDL_KEYUP, SDL_IGNORE);
  SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
  lista_streams=new List();
}

EventManager::~EventManager(){
  lista_streams->empty(true);
  delete lista_streams;
}

void EventManager::addStream(EventStream * s){
  lista_streams->insert(lista_streams->getEnd(), s);
}

void EventManager::removeStream(EventStream * s){
  lista_streams->remove(lista_streams->indexOf(s));
}

void EventManager::pumpEvents(bool wait=false){
  SDL_Event event;
  Index * stream;
  bool got=false;

  do{
    while (SDL_PollEvent(&event)>0){
      got=true;
      switch (event.type){
        case SDL_KEYDOWN:{
          stream=lista_streams->getFirst();
          while ((stream!=lista_streams->getEnd()) &&
                ((EventStream*)(lista_streams->getObject(stream)))->put(event.key.keysym.sym)){
            stream=lista_streams->getNext(stream);
          }
        }
        default:break;
      }
    }
    if (wait && !got) SDL_WaitEvent(NULL);
  }while (wait && !got);
}

