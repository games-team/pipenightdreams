/***************************************************************************
                          verticalbowl.h  -  description
                             -------------------
    begin                : Thu Dec 28 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VERTICAL_BOWL_H
#define VERTICAL_BOWL_H

#include "vertical.h"

class VerticalBowl: public Vertical{

  public:

    VerticalBowl();

    void incFullLevel(CardinalPoint input,unsigned int amount);

    void paint(VideoManager * vm);

  protected:

    static const int BowlCapacity=500;
    int bowl_level;

};
#endif

