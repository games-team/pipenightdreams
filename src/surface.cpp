/***************************************************************************
                          surface.cpp  -  description
                             -------------------
    begin                : Sat Oct 14 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "surface.h"
#include <stdio.h>

Surface::Surface(){
  surface=NULL;
}

Surface::Surface(int width, int height){
  SDL_Rect rect;
  SDL_Surface * aux_surface;

  rect.x=0;
  rect.y=0;
  rect.w=width;
  rect.h=height;

  aux_surface=SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_SRCALPHA, width, height,
                               32, 0xff, 0x00ff,0x0000ff,0x000000ff);
  surface=SDL_DisplayFormatAlpha(aux_surface);
	SDL_FreeSurface(aux_surface);
}

Surface::~Surface(){
  if (surface) SDL_FreeSurface(surface);
}

int Surface::width(){
  if (surface) return surface->w;
  return 0;
}

int Surface::height(){
  if (surface) return surface->h;
  return 0;
}

