/***************************************************************************
                          cross.h  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CROSS_H
#define CROSS_H

#include "pipe.h"

class Cross: public Pipe{

  public:

    Cross();
    ~Cross();

    bool hasConnection(CardinalPoint con);

    bool isRemovable();

    bool hasLiquid();

    void restrictAsOutput(CardinalPoint con);

    bool isRestrictedAsOutput(CardinalPoint con);

    CardinalPoint getOutput(CardinalPoint input);

    void incFullLevel(CardinalPoint input,unsigned int amount);

    int getFullLevel(CardinalPoint input);

    Pointer * getPointer();

    void paint(VideoManager * vm);

  protected:

    int extra_full_level;
    int extra_used_input;
    CardinalPoint extra_ro;
};

#endif

