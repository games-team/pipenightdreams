/***************************************************************************
                          tank.cpp  -  description
                             -------------------
    begin                : Fri Dec 29 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "tank.h"

#define MAX_STATE 20

Tank::Tank(unsigned int full_level):AnimatedCanvas(){
  this->full_level=full_level%(Capacity-1);
  state=0;
  changed=true;
}

Tank::~Tank(){}

void Tank::decFullLevel(unsigned int amount){
  if (full_level>0 && (unsigned int)full_level>amount)
    full_level-=amount;
  else full_level=0;
  changed=true;
}

bool Tank::isEmpty(){
  return full_level<=0;
}

bool Tank::isChanged(){
  return changed;
}

void Tank::paint(VideoManager * vm){
  int f=(int)((float)TankHeight*(float)full_level/(float)Capacity);
  Image * ima;

  // Borra
  vm->fillRect(x,y,TankWidth,TankHeight-f, 0,0,0,OPAQUE);
  //Dibuja el liquido
  vm->fillRect(x,y+TankHeight-f, TankWidth, f, 103,234,16,OPAQUE);
  //Pinta el frente
  ima=(vm->getImageManager())->getImage(new Str("tank.png"));
  vm->blit(ima,x,y);
  changed=false;
}

void Tank::tick(){
  state=(state+1)%MAX_STATE;
}


