/***************************************************************************
                          image.h  -  description
                             -------------------
    begin                : Sat Aug 19 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef IMAGE_H
#define IMAGE_H

#include "graphic.h"
#include "str.h"
#include "SDL.h"

class Image: public Graphic{

  public:

    /** Constructor.
    @param filename La imagen filename es cargada. Debe ser distinto de NULL.
    */
    Image(Str * filename=NULL);

    Image(Image * im);

    /** Destructor.*/
    virtual ~Image(){};

  protected:

    void load(Str * filename);
};
#endif

