/***************************************************************************
                          pipenightdreams.h  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PIPE_NIGHT_DREAMS_H
#define PIPE_NIGHT_DREAMS_H

#include "videomanager.h"
#include "imagemanager.h"
#include "eventmanager.h"
#include "object.h"
#include "board.h"
#include "player.h"
#include "systemstream.h"

#define FPS 20

class PipeNightDreams: public Object{

  public:

    PipeNightDreams(bool fullscreen);
    ~PipeNightDreams();

    /** The method to assign the data dir */
    void setDataDir(Str * dir_name);

    void setStartingLevel(int level);


    /** The method that run the game. */
    void run();

  protected:

    /** Introduction display.*/
    void showIntro();
    /** Options Menu display.*/
    void showOptions();
    /** Level execution.*/
    void play();
    /** Main Menu.*/
    void showMainMenu();
    /** Game Over screen.*/
    void showGameOver();
    /** Hall of Fame screen.*/
    void showHallOfFame();
    /** Next Level Screen*/
    void showNextLevel();
    /**Try Again Screen*/
    void showTryAgain();
    /** Final screen*/
    void showFinal();
    /** Load main graphics*/
    void load();

    VideoManager * video_manager;
    ImageManager * image_manager;
    EventManager * event_manager;

  private:

    bool game_over;
    bool quit;
    SystemStream * sys;
    Str * data_dir;
    Str * images_dir;
    Board * board;
    Player * player;
    Image * nextlevel;
    Image * tryagain;
    Image * pressenter;
    int starting_level;
};

#endif

