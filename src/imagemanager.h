/***************************************************************************
                          imagemanager.h  -  description
                             -------------------
    begin                : Sat Aug 19 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IMAGE_MANAGER_H
#define IMAGE_MANAGER_H

#include "object.h"
#include "str.h"
#include "hash.h"
#include "image.h"

/** Es la encargada de manejar la carga y liberación de grupos de
    imágenes.
*/

class ImageManager: public Object{

  public:
    /** Constructor default. */
    ImageManager();

    /** Destructor.*/
    virtual ~ImageManager();

    /** Asigna el directorio base de las imagenes.*/
    void setBaseDir(Str * dir);

    /** Precarga una imagen. En caso de que el archivo no exista
        no la precarga. Si una imagen con el mismo nombre ya existe
        el comportamiento es indefinido.
    */
    Image * preLoadImage(Str * filename);

    /** Retorna una imagen. La carga si no la tiene ya cargada.
        retorna NULL si el archivo no existe.*/
    Image * getImage(Str * filename);

    /** Agrega un directorio a la lista de busqueda de imagenes*/
    void addDir(Str * dir);

    /** Vacia la lista de directorios.*/
    void clearDirs();

    /** Agrega una lista de directorios*/
    void addDirList(List * dir_list);

    void empty();

  protected:

    Hash * hash;
    List * dirs;
    Str * base_dir;

};

#endif


