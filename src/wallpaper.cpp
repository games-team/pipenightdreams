/***************************************************************************
                          wallpaper.cpp  -  description
                             -------------------
    begin                : Wed Mar 21 2001
    copyright            : (C) 2001 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "wallpaper.h"

WallPaper::WallPaper(Image * image):Background(){
  this->image=image;
  image->setAlpha(OPAQUE);
}

WallPaper::~WallPaper(){
  //delete image;
}

void WallPaper::repaint(VideoManager * vm, int x, int y, int w, int h){
    /** Pinta el rectangulo (rx,ry,rw,rh) de la surface s
        en la posicion (x,y)*/
  vm->blit(image, this->x+x, this->y+y, x, y, w, h);
}

void WallPaper::paint(VideoManager * vm){
  vm->blit(image, x, y);
}


