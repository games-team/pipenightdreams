/***************************************************************************
                          animatedcanvas.h  -  description
                             -------------------
    begin                : Wed Dec 6 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

 #ifndef ANIMATED_CANVAS_H
 #define ANIMATED_CANVAS_H

 #include "canvas.h"

 /** Los canvas deben mantener alg�n tipo de estado que cambiar�
     o no cuando se les hagan llamadas a tick().
 */

 class AnimatedCanvas : public Canvas{

  public:
  /** Constructor*/
  AnimatedCanvas():Canvas(){};

	virtual ~AnimatedCanvas(){};

  virtual void tick()=0;
 };
 #endif


