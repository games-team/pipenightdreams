/***************************************************************************
                          imagemanager.cpp  -  description
                             -------------------
    begin                : Sat Aug 19 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "imagemanager.h"

ImageManager::ImageManager():Object(){
  hash=new Hash();
  dirs=new List();
  base_dir=new Str("");
  dirs->insert(dirs->getFirst(), new Str(""));
}

ImageManager::~ImageManager(){
  hash->empty(true);
  dirs->empty(true);
  delete base_dir;
  delete hash;
  delete dirs;
}

void ImageManager::setBaseDir(Str * dir){
  delete base_dir;
  base_dir=dir;
}

Image * ImageManager::preLoadImage(Str * filename){
  Str * aux;
  Str * sub_dir=NULL;
  FILE * file;
  Image * ima=NULL;
  Index * ind = dirs->getFirst();
  while (!ima && ind!=dirs->getEnd()){
    sub_dir=new Str((Str*)(dirs->getObject(ind)));
    sub_dir->concat("/");
    sub_dir->concat(filename);
    aux=new Str(base_dir);
    aux->concat("/");
    aux->concat(sub_dir);
    if ((file=fopen(aux->get(), "r"))){
      fclose(file);
      ima = new Image(new Str(aux));
      hash->add(sub_dir, ima);
      delete aux;
      delete filename;
    }else{
      delete aux;
      delete sub_dir;
    }
    ind=dirs->getNext(ind);
  }
  return (ima);
}

Image * ImageManager::getImage(Str * filename){
  Image * ima=NULL;
  Index * ind = dirs->getFirst();
  Str * aux;
  Str slash = Str("/");
  while ((!ima) && (ind!=dirs->getEnd())){
    aux = new Str((Str*)(dirs->getObject(ind)));
    aux->concat(&slash);
    aux->concat(filename);
    ima = (Image *)hash->find(aux);
    ind=dirs->getNext(ind);
  }
  if (!ima) ima=preLoadImage(new Str(filename));
  if (ima){
    delete filename;
    return ima;
  }
  printf("Fatal error: File %s not found\n", filename->get());
  delete filename;
  exit(-1);
}

void ImageManager::addDir(Str * dir){
  dirs->insert(dirs->getEnd(), dir);
}

void ImageManager::addDirList(List * dir_list){
  Index * ind=dir_list->getFirst();
  while (!dir_list->isEnd(ind)){
    addDir((Str*)(dir_list->getObject(ind)));
    ind=dir_list->getNext(ind);
  }
  dir_list->empty(false);
  delete dir_list;
}

void ImageManager::clearDirs(){
  dirs->empty(true);
  dirs->insert(dirs->getFirst(), new Str(""));
}

void ImageManager::empty(){
  hash->empty(true);
  clearDirs();
}

