/***************************************************************************
                          pointer.h  -  description
                             -------------------
    begin                : Thu Sep 14 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POINTER_H
#define POINTER_H

#include "canvas.h"

class Board;

class Pointer: public Canvas{

  public:
    static const int width=60;
    static const int height=60;

    /** Constructor default.*/
    Pointer(int row=0, int column=0);

    /** Destructor.*/
    ~Pointer();

    int getRow();
    int getColumn();

    void paint(VideoManager * vm);

  protected:

    friend class Board;

    inline void setRow(int row);
    inline void setColumn(int column);
    inline void setRowColumn(int row, int column);
    inline void setMoved(bool flag);
    inline bool moved();

    Str * image_name;
    Image * ima;

    int row, column;
    bool moved_flag;
};

#endif

