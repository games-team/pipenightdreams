/***************************************************************************
                          mosaic.cpp  -  description
                             -------------------
    begin                : Wed Mar 21 2001
    copyright            : (C) 2001 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mosaic.h"

Mosaic::Mosaic(Image * image):Background(){
  this->image=image;
  image->setAlpha(OPAQUE);
}

Mosaic::~Mosaic(){
  //delete image;
}

void Mosaic::repaint(VideoManager * vm, int x, int y, int w, int h){
  vm->blit(image, this->x+x, this->y+y);
}

void Mosaic::paint(VideoManager * vm){
  int i,j;
  for (i=0;i<BoardColumns;i++)
    for (j=0;j<BoardRows;j++)
      vm->blit(image, i*PipeWidth+x, j*PipeHeight+y);
}

