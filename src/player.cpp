/***************************************************************************
                          player.cpp  -  description
                             -------------------
    begin                : Sat Sep 30 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "player.h"

#define MAX_LIVES 8

Player::Player(EventStream * es):AnimatedCanvas(){
  stream=es;
  score=0;
  start_row=0;
  start_column=0;
  change=true;
  rest_coef=0;
  fixed_coef=0;
  nlives=StartLives;
  score=new Score();
  queue=new PipeQueue();
  required=new Score();
}

Player::~Player(){
  delete queue;
  delete score;
  delete required;
}

int Player::width(){
  return PlayerWidth;
}
int Player::height(){
  return PlayerHeight;
}


void Player::setPos(int x, int y){
  Canvas::setPos(x,y);
  queue->setPos(x,y);
  score->setPos(x+10,y+200);
  required->setPos(x+10,y+260);
}

void Player::setStartRowColumn(int row, int column){
  start_row=row;
  start_column=column;
}

void Player::setBoard(Board * bd){
  board=bd;

  pointer=queue->getPipe(0)->getPointer();
  board->addPointer(pointer);
  board->movePointer(pointer,start_row,start_column);

  stream->clear();
}

void Player::incLives(){
  nlives++;
  change=true;
}

void Player::decLives(){
  if (nlives>0) nlives--;
  change=true;
}

unsigned int Player::lives(){
  return nlives;
}

bool Player::isChanged(){
  return change || queue->isChanged() || score->isChanged() || required->isChanged();
}

void Player::play(){
  Event event;

  event=stream->get();
  switch (event){
    case UP:{
      if (pointer->getRow()> 0)
        board->movePointer(pointer, pointer->getRow()-1, pointer->getColumn());
      break;
    }
    case DOWN:{
      if (pointer->getRow()<BoardRows-1)
        board->movePointer(pointer, pointer->getRow()+1, pointer->getColumn());
      break;
    }
    case LEFT:{
      if (pointer->getColumn()>0)
        board->movePointer(pointer, pointer->getRow(), pointer->getColumn()-1);
      break;
    }
    case RIGHT:{
      if (pointer->getColumn()<BoardColumns-1)
                board->movePointer(pointer, pointer->getRow(), pointer->getColumn()+1);
      break;
    }
    case HIT:{
      int r=pointer->getRow(),c=pointer->getColumn();
      if (board->isRemovable(r,c)){
        board->removePointer(pointer);
        Pipe * aux = queue->getHead();
        aux->setOwner(this);
        board->setPipe(aux, r, c);
        pointer=queue->getPipe(0)->getPointer();
        board->movePointer(pointer,r, c);
        board->addPointer(pointer);
      }
      break;
    }
    case GO:{
      board->setMaxSpeed();
    }
    default:break;
  }
}

void Player::paint(VideoManager * vm){

  //Pintar el background
  if (change){
    //Dibuja las vidas
    vm->fillRect(x+LivesX,y+LivesY,MAX_LIVES*LivesWidth,LivesHeight, 0,0,0,255);

    //Dibuja el liquido
    vm->fillRect(x+LivesX,y+LivesY, lives()*LivesWidth, LivesHeight, 103,234,16,255);
    vm->blit(vm->getImageManager()->getImage(new Str("lives_one.png")), x,y+215);

    vm->blit(vm->getImageManager()->getImage(new Str("panel.png")), x,y+275);
    change=false;
  }
  // Pintar puntaje
  if (score->isChanged()){
    score->paint(vm);
    vm->blit(vm->getImageManager()->getImage(new Str("score_one.png")),x,y+185);
  }

  // Pintar required
  if (required->isChanged()){
    required->paint(vm);
    vm->blit(vm->getImageManager()->getImage(new Str("required_one.png")),x,y+240);
  }

  //Pintar la cola
  if (queue->isChanged())
    queue->paint(vm);
}

void Player::setRestrictionCoef(unsigned int coef){
  queue->setRestrictionCoef(coef);
}

void Player::setFixedCoef(unsigned int coef){
  queue->setFixedCoef(coef);
}

void Player::tick(){
  score->tick();

  unsigned int still_required=board->getStillRequired();

  if ((int)still_required<required->getValue())
    required->dec(required->getValue()-(int)still_required);
  else
    if ((int)still_required>required->getValue())
      required->inc((int)still_required-required->getValue());
  required->tick();
}

void Player::incScore(Bonus bonus){
  switch (bonus){
    case NormalBonus:{
      score->inc(NORMAL_BONUS_VALUE);
      break;
    }
    case SuperBonus:{
      score->inc(SUPER_BONUS_VALUE);
      break;
    }
    case UltraBonus:{
      score->inc(ULTRA_BONUS_VALUE);
      break;
    }
    case HyperBonus:{
      score->inc(HYPER_BONUS_VALUE);
      break;
    }
    default:break;
  }
}

unsigned int Player::scoreValue(){
  return score->getValue();
}

