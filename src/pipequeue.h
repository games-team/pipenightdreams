/***************************************************************************
                          pipequeue.h  -  description
                             -------------------
    begin                : Sat Sep 30 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PIPE_QUEUE_H
#define PIPE_QUEUE_H

#include "animatedcanvas.h"
#include "pipe.h"
#include "videomanager.h"

class PipeQueue : public AnimatedCanvas{

  public:

    static const int MaxPipes=3;

    PipeQueue();
    ~PipeQueue();

    int width();

    int height();

    /** Retorna el Pipe que se encuentra en la cabeza
        de la cola y lo quita agregando luego uno por la cola.*/
    Pipe * getHead();

    /** Retorna el pipe en la posicion.*/
    Pipe * getPipe(int pos);

    /** Asigna una probabilidad entre 1..100 de agregar restricciones
       de direccion.*/
    void setRestrictionCoef(unsigned int coef);

    /** Asigna una probabilidad entre 1..100 de agregar pipes fijos.*/
    void setFixedCoef(unsigned int coef);

    bool isChanged();

    /** paint heredado.*/
    void paint(VideoManager * vm);

    void tick(){};

  protected:

    void fillUp();

    Pipe * queue[MaxPipes];
    int index;
    bool change;
    unsigned int res_coef;
    unsigned int fixed_coef;
    bool filled;

    Pipe * generatePipe();

};

#endif

