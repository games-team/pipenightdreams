/***************************************************************************
                          vertical.cpp  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "vertical.h"
#include "pointervertical.h"

Vertical::Vertical(){
  p=new PointerVertical();
}

Vertical::~Vertical(){
  delete p;
}

bool Vertical::hasConnection(CardinalPoint con){
  return (con==South || con==North);
}

CardinalPoint Vertical::getOutput(CardinalPoint input){
  if (input==ro) return Void;
  switch (input){
    case South: return North;
    case North:return South;
    default:return Void;
  }
}

void Vertical::restrictAsOutput(CardinalPoint con){
  if (con==North || con==South) ro=con;
}

bool Vertical::isRestrictedAsOutput(CardinalPoint con){
  return (con==ro);
}

void Vertical::incFullLevel(CardinalPoint input,unsigned int amount){
  if ((input==North || input==South) && input!=ro)
    if ((used_input==Void) || (used_input==input)){
      full_level+=amount;
      used_input=input;
    }
}

int Vertical::getFullLevel(CardinalPoint input){
  if (used_input==input)
    return full_level;
  return -1;
}

Pointer * Vertical::getPointer(){
  return p;
}

void Vertical::paint(VideoManager * vm){

  Image * ima;
  if (fixed)
    ima=(vm->getImageManager())->getImage(new Str("vertical_b.png"));
  else
    ima=(vm->getImageManager())->getImage(new Str("vertical.png"));
  vm->blit(ima, x, y);

  if (ro!=Void) paintRestriction(vm, ro);

  paintFlow(vm);

  if (bonus!=NormalBonus) paintBonus(vm, bonus);

  //if (last) paintExclamation(vm);

}

void Vertical::paintFlow(VideoManager * vm){
  if (full_level>0){
    Image * aux=(vm->getImageManager())->getImage(new Str("flow.png"));
    int xof=x+PipeWidth/2-aux->width()/2;
    int yof=aux->height()/2;
    int total, f;
    vm->setClipping(x, y, PipeWidth, PipeHeight);
    vm->enableClipping(true);
    if (used_input==North){
      total=y-yof;
      f=1;
    }
    else{
      total=PipeHeight+y-yof;
      f=-1;
    }
    for (int i=0; i<full_level+Gran;i+=Gran)
      vm->blit(aux, xof, total+f*(int)((float)i/(float)full()*(float)PipeHeight));
vm->enableClipping(false);   }
}

