/***************************************************************************
                          random.cpp  -  description
                             -------------------
    begin                : Tue Dec 12 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "random.h"
#include <stdio.h>

int Random::getRandomNumber(int low, int high){
  int result=(int)((float)(high-low+1)*(float)random()/(RAND_MAX))+low;
  return result;
}

