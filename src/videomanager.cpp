/***************************************************************************
                          videomanager.cpp  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "videomanager.h"

int max(int a, int b){
  return (a>b?a:b);
}

int min(int a, int b){
  return (a<b?a:b);
}

VideoManager::VideoManager(int flags):Graphic(){
  int depth;


  Uint32 video_flags=0;
  if (flags && FullScreen) video_flags|=SDL_FULLSCREEN;

  //Init video in 640x480x16 by default

  if ((depth=SDL_VideoModeOK(640, 480, 16, 0)))
    surface=SDL_SetVideoMode(640, 480, depth, video_flags);
  else printf("Modo no soportado\n");

  im=new ImageManager();
  nrects=0;
}

VideoManager::~VideoManager(){
  surface=NULL;
  delete im;
}

void VideoManager::setCaption(Str * str){
  SDL_WM_SetCaption(str->get(), NULL);
  delete str;
}

void VideoManager::blit(Surface * s, int x, int y, int rx, int ry, int rw, int rh){
  SDL_Rect rsrc, rdst;

  rsrc.x=rx;
  rsrc.y=ry;
  rsrc.w=rw;
  rsrc.h=rh;

  rdst.x=x;
  rdst.y=y;

  SDL_BlitSurface(s->surface, &rsrc, surface, &rdst);
  addRect(rdst.x, rdst.y, (unsigned int)rdst.w, (unsigned int)rdst.h);
}

void VideoManager::blit(Surface * s, int x, int y){
  SDL_Rect rdst;

  rdst.x=x;
  rdst.y=y;
  SDL_BlitSurface(s->surface, NULL, surface, &rdst);
  addRect(rdst.x, rdst.y, (unsigned int)rdst.w, (unsigned int)rdst.h);
}

void VideoManager::flush(){
  SDL_UpdateRects(surface, nrects, rects);
  nrects=0;
}

ImageManager * VideoManager::getImageManager(){
  return im;
}

void VideoManager::fillRect(int x, int y, int w, int h, int r, int g, int b, int a){
  Graphic::fillRect(x,y,w,h,r,g,b,a);

  SDL_Rect rect;
  rect.x=x;
  rect.y=y;
  rect.w=w;
  rect.h=h;
  addRect(rect.x, rect.y, (unsigned int)rect.w, (unsigned int)rect.h);
}

void VideoManager::addRect(int x, int y, unsigned int w, unsigned int h){
  SDL_Rect rect;
  rect.x=x;
  rect.y=y;
  rect.w=w;
  rect.h=h;
  int i=0;
  bool ready=false;
  while (i<nrects && !ready){
    if ((rects[i].x==rect.x && rects[i].y==rect.y)     ||
      ((rects[i].x + rects[i].w)==(rect.x + rect.w) && rects[i].y==rect.y)   ||
      (rects[i].x==rect.x && (rects[i].y + rects[i].h)==(rect.y + rect.w))   ||
      ((rects[i].x + rects[i].w)==(rect.x + rect.w) && (rects[i].y + rects[i].h)==(rect.y + rect.w))){
        rects[i].x = min(rects[i].x, rect.x);
        rects[i].y = min(rects[i].y, rect.y);
        rects[i].w = max(rects[i].w, rect.w);
        rects[i].h = max(rects[i].h, rect.h);
        ready=true;
    }
    i++;
  }
  if (!ready) {
    rects[nrects].x = rect.x;
    rects[nrects].y = rect.y;
    rects[nrects].w = rect.w;
    rects[nrects].h = rect.h;
    nrects++;
  }
}


