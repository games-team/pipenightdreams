/***************************************************************************
                          object.h  -  description
                             -------------------
    begin                : Thu Apr 20 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

#ifndef OBJECT_H
#define OBJECT_H

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <typeinfo>

#define PI 3.1415
#define DSIZE 1024*1024

class Object{
  public:
    Object(){
     /* if (index<DSIZE)
        all[index++]=this;*/
    }

    virtual ~Object(){
    /*  bool ready=false;
      for (int i=0;i<DSIZE && !ready;i++){
        if (all[i]==this){
          all[i]=NULL;
          ready=true;
        }
      } */
    }

    virtual void reportLeaks(){
      int end=DSIZE;
      if (index<DSIZE)
        end=index;
      else
        printf("Not complete\n");

      for (int i=0;i<end;i++){
        if (all[i]!=NULL)
          printf("%s\n", typeid(*(all[i])).name());
      }
    }
  protected:
    static int index;
    static Object * all[];
};


#endif

