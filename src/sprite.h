/***************************************************************************
                          sprite.h  -  description
                             -------------------
    begin                : Thu Jan 17 2002
    copyright            : (C) 2002 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SPRITE_H
#define SPRITE_H

#include "graphic.h"
#include "str.h"
#include "SDL.h"

class List;
class Surface;
class ImageManager;
class Image;

class Sprite: public Graphic{

  public:

    /** Constructor.
    @param filenamebase El sprite es cargado usando <filenamebase>.spr <filenamebase>.png
    */
    Sprite(Str * filenamebase, Str * filenameext, int nFrames, ImageManager * im);

    /** Destructor.*/
    virtual ~Sprite();

    virtual int nFrames();

    virtual int frameWidth(ImageManager * im);
    virtual int frameHeight(ImageManager * im);

    virtual Surface * frame(ImageManager * im, int n);


  protected:
    Image ** frames;
		int n_frames;
		Str * filenamebase;
		Str * filenameext;
};
#endif
