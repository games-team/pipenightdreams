/***************************************************************************
                          pointerverticalbowl.h  -  description
                             -------------------
    begin                : Thu Dec 28 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POINTER_VERTICAL_BOWL_H
#define POINTER_VERTICAL_BOWL_H

#include "pointer.h"

class PointerVerticalBowl: public Pointer{

  public:
    PointerVerticalBowl():Pointer(){
      image_name=new Str("pbowl_v.png");
    }
};

#endif

