/***************************************************************************
                          background.h  -  description
                             -------------------
    begin                : Wed Mar 21 2001
    copyright            : (C) 2001 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "canvas.h"

class Background;

#include "board.h"
#include "pipe.h"

class Background: public Canvas{

  public:
    Background():Canvas(){};

    int width(){return BoardColumns*PipeWidth;}
    int height(){return BoardRows*PipeHeight;}

    virtual void repaint(VideoManager * vm, int x, int y, int w, int h)=0;
};
#endif

