/***************************************************************************
                          sprite.cpp  -  description
                             -------------------
    begin                : Thu Jan 17 2002
    copyright            : (C) 2002 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "sprite.h"
#include "list.h"
#include "surface.h"
#include "imagemanager.h"

Sprite::Sprite(Str * filenamebase, Str * filenameext, int nFrames, ImageManager * im){
  frames = (Image**) malloc(nFrames*(sizeof(Image*)));

  this->n_frames=nFrames;
  this->filenamebase=filenamebase;
  this->filenameext=filenameext;
  char aux[256];

  for (int i=0;i<nFrames;i++){
    sprintf(aux, "%s%04i%s", filenamebase->get(), i, filenameext->get());
    frames[i]=im->getImage(new Str(aux));
  }
}

Sprite::~Sprite(){
  delete frames;
  if (filenamebase) delete filenamebase;
  if (filenameext) delete filenameext;
}

int Sprite::nFrames(){
  return n_frames;
}

int Sprite::frameWidth(ImageManager * im){
  return frames[0]->width();
}

int Sprite::frameHeight(ImageManager * im){
  return frames[0]->height();
}

Surface * Sprite::frame(ImageManager * im, int n){
  return frames[n];
}
