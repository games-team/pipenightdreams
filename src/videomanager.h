/***************************************************************************
                          videomanager.h  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                              *
 ***************************************************************************/

#ifndef VIDEO_MANAGER_H
#define VIDEO_MANAGER_H

#include "graphic.h"
#include "SDL.h"
#include "surface.h"
#include "imagemanager.h"
#include "list.h"
#include "str.h"

class VideoManager: public Graphic{

  public:

    static const int max_rects=512;

    enum VideoFlag {None=0, FullScreen=1};

    /** The constructor initializes the video.*/
    VideoManager(int flags=0);

    /** Closes the video. */
    ~VideoManager();

    void setCaption(Str * str);

    /** Pinta el rectangulo (rx,ry,rw,rh) de la surface s
        en la posicion (x,y)*/
    void blit(Surface * s, int x, int y, int rx, int ry, int rw, int rh);

    /** Pinta la surface en la posicion (x,y)*/

    void blit(Surface * s, int x, int y);

    /**  Llena un rectangulo (x,y,w,h) con el color de componentes (r,g,b,a)*/
    void fillRect(int x, int y, int w, int h, int r, int g, int b, int a);

    /** Realiza la actualización en pantalla de todos los rectangulos.*/
    void flush();

    /** Retorna el image manager asociado.*/
    ImageManager * getImageManager();

  protected:

    class Rect;

    inline void addRect(int x, int y, unsigned int w, unsigned int h);

    ImageManager * im;
    SDL_Rect rects[max_rects];

    int nrects;
};

#endif


