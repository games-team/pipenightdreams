/***************************************************************************
                          player.h  -  description
                             -------------------
    begin                : Sat Sep 30 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PLAYER_H
#define PLAYER_H

#include "animatedcanvas.h"
#include "pipe.h"
#include "pipequeue.h"
#include "eventstream.h"
#include "board.h"
#include "score.h"

class Player : protected AnimatedCanvas{

  public:

    static const int PlayerWidth=88;
    static const int PlayerHeight=240;
    static const int StartLives=3;
    static const int LivesX=10;
    static const int LivesY=230;
    static const int LivesWidth=9;
    static const int LivesHeight=9;

    Player(EventStream * es);
    virtual ~Player();

    int width();
    int height();

    void setPos(int x, int y);
    void setStartRowColumn(int row, int column);
    void setBoard(Board * bd);

    inline void setRestrictionCoef(unsigned int coef);
    inline void setFixedCoef(unsigned int coef);

    void incLives();
    void decLives();
    unsigned int lives();
    bool isChanged();
    void incScore(Bonus bonus);
    void setRequired(int required);
    void incRequired(int inc);
    void decRequired(int dec);
    unsigned int scoreValue();

    void play();
    void paint(VideoManager * vm);
    void tick();

  protected:

    unsigned int nlives;
    unsigned int rest_coef;
    unsigned int fixed_coef;
    int start_row, start_column;
    bool change;
    Pointer * pointer;
    PipeQueue * queue;
    EventStream * stream;
    Board * board;
    Score * score;
    Score * required;
};

#endif

