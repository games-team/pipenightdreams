/***************************************************************************
                          score.h  -  description
                             -------------------
    begin                : Sun Mar 11 2001
    copyright            : (C) 2001 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SCORE_H
#define SCORE_H

#include "animatedcanvas.h"

class Score : public AnimatedCanvas{

  public:

    Score(int value=0);
    virtual ~Score();

    void inc(unsigned int value);

    void dec(unsigned int value);

    void sumUp();

    int getValue();

    bool isChanged();

    void paint(VideoManager * vm);

    void tick();

  private:
    static const unsigned int TickValue=1;
    static const unsigned int ScoreWidth = 80;
    static const unsigned int ScoreHeight = 15;
    static const unsigned int Digits = 8;

    unsigned int value;
    int delta;
    bool changed;
};

#endif

