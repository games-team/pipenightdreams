/***************************************************************************
                          paper.cpp  -  description
                             -------------------
    begin                : Fri Jan 18 2002
    copyright            : (C) 2002 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "paper.h"
#include "sprite.h"
#include "random.h"

const int Paper::DeltaY=5;
const int Paper::NFrames=5;

Paper::Paper(int maxY, PaperColor color, ImageManager * im):AnimatedCanvas(){

  this->maxY=maxY;
  Random ran=Random();
  frame=ran.getRandomNumber(0, NFrames);
  l_width=20;
  l_height=12;

  Str * filenamebase;

  switch (color){
    case Red:{filenamebase =new Str("red_paper");break;}
    case Blue:{filenamebase =new Str("blue_paper");break;}
    case Green:{filenamebase =new Str("green_paper");break;}
  }
  sprite=new Sprite(filenamebase, new Str(".png"), NFrames, im);
}

Paper::~Paper(){
  delete sprite;
}

int Paper::width(){
  return l_width;
}

int Paper::height(){
 return l_height;
}


void Paper::paint(VideoManager * vm){
  vm->blit(sprite->frame(vm->getImageManager(), frame), x, y);
}

void Paper::tick(){
  frame=(frame+1)%NFrames;
  y=(y+DeltaY)%maxY;
}
