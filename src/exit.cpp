/***************************************************************************
                          exit.cpp  -  description
                             -------------------
    begin                : Sat Oct 21 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "exit.h"

Exit::Exit(CardinalPoint connection):Pipe(){
  con=connection;
}

Exit::~Exit(){}

bool Exit::hasConnection(CardinalPoint con){
  return (this->con==con);
}

CardinalPoint Exit::getOutput(CardinalPoint input){
  return Void;
}

bool Exit::isRemovable(){
  return false;
}

void Exit::incFullLevel(CardinalPoint input,unsigned int amount){
  full_level+=amount;
  used_input=Void;
}

int Exit::getFullLevel(CardinalPoint input){
  return this->full_level;
}

Pointer * Exit::getPointer(){
  return NULL;
}

void Exit::paint(VideoManager * vm){
  Image * aux, *ima;

  if (con==East || con==West)
    ima=(vm->getImageManager())->getImage(new Str("exit_h.png"));
  else
    ima=(vm->getImageManager())->getImage(new Str("exit_v.png"));
  aux=new Image(ima);
  switch (con){
    case South:{aux->flip(HAxis);break;}
    case West:{aux->flip(VAxis);break;}
    default:break;
  }
  vm->blit(aux, x, y);
  delete aux;

  if (full_level>0){
    ima=(vm->getImageManager())->getImage(new Str("flow.png"));
    vm->setClipping(x, y, PipeWidth, PipeHeight);
    vm->enableClipping(true);
    switch (con){
      case North:case South:{
        int xof=x+PipeWidth/2-ima->width()/2;
        int yof;
        float factor=(float)PipeHeight/(float)(full());
        if (con==North) yof=y-ima->height()/2;
        else{
          yof=y+PipeHeight-ima->height()/2;
          factor =-factor;
        }
        for (int i=0;i<full_level/2+Gran;i+=Gran)
          vm->blit(ima, xof, (int)(yof+(float)i*factor));
        break;
      }
      case East: case West:{
        int xof;
        int yof=y+PipeHeight/2-ima->height()/2;;
        float factor=(float)PipeWidth/(float)(full());
        if (con==West) xof=x-ima->width()/2;
        else{
          xof=x+PipeWidth-ima->width()/2;
          factor =-factor;
        }
        for (int i=0;i<full_level/2+Gran;i+=Gran)
          vm->blit(ima, (int)(xof+(float)i*factor), yof);
        break;
      }
      default:break;
    }
    vm->enableClipping(false);
  }
}

