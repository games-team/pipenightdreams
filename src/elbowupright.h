/***************************************************************************
                          elbowupright.h  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ELBOW_UP_RIGHT_H
#define ELBOW_UP_RIGHT_H

#include "pipe.h"

class ElbowUpRight: public Pipe{

  public:

    ElbowUpRight();
    ~ElbowUpRight();

    bool hasConnection(CardinalPoint con);

    CardinalPoint getOutput(CardinalPoint input);

    void restrictAsOutput(CardinalPoint con);

    bool isRestrictedAsOutput(CardinalPoint con);

    void incFullLevel(CardinalPoint input,unsigned int amount);

    int getFullLevel(CardinalPoint input);

    Pointer * getPointer();

    void paint(VideoManager * vm);
};

#endif

