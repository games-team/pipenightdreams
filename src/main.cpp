/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Thu Aug 17 15:27:08 ART 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "pipenightdreams.h"
#include "hash.h"
#include "str.h"

int main(int argc, char *argv[])
{
  Str * data_dir=NULL;
  int level=1;
  bool fullscreen=false;
  int i=1;


  while (i<argc){
    if (!strcmp("--datadir", argv[i])){
      if (data_dir) delete data_dir;
      data_dir=new Str(argv[++i]);
    }else
      if (!strcmp("--fullscreen", argv[i]))
        fullscreen=true;
      else{
        printf("Invalid Data Dir\nUse pipenightdreams [--datadir <DATA DIR>] [--fullscreen]\n");
        printf("Using default: %s\n", GAME_DATADIR);
      }
    i++;
  }

  if (!data_dir) data_dir=new Str(GAME_DATADIR);

  PipeNightDreams * g = new PipeNightDreams(fullscreen);
  g->setDataDir(data_dir);
  g->setStartingLevel(level);
  g->run();
  delete g;
}
