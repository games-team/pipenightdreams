/***************************************************************************
                          level.h  -  description
                             -------------------
    begin                : Fri Dec 8 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LEVEL_H
#define LEVEL_H

#include "object.h"
#include "str.h"
#include "entry.h"
#include "exit.h"
#include "list.h"
#include "board.h"

class Level: public Object{
  public:

    /** Constructor default.*/
    Level();

    /** Destructor*/
    ~Level();

    /** Returns the entry*/
    Entry * getEntry(int& row, int& column);

    /** Returns the exit*/
    Exit * getExit(int& row, int& column);

    /** Returns the probability coeficient of fixed pipes*/
    int getFixedCoef();

    /** Returns the probability coeficient of restricted pipes*/
    int getRestrictionCoef();

    /** Returns the time to wait until start*/
    int getStartDelay();

    /** Returns the speed*/
    int getSpeed();

    /** Returns the number of required pipes.*/
    int getRequired();

    /** Returns a list with the graph directories*/
    List * getGraphDirs();

    /** Returns the first pipe index*/
    int getPipeIndex();

    /** Returns the background type*/
    BackgroundType getBackgroundType();

    /** Returns the next pipe, NULL if it does not exist*/
    Pipe * getPipe(int& i, int& row, int& column);

    /** Carga el nivel en el board.*/
    int load(Str * filename);

  protected:

    enum PipeType {ElbowNW, ElbowNE, ElbowSW, ElbowSE,
                   BowlH, BowlV, Vert, Horiz, CrossPipe};

    int numberAssignment();
    Str * stringAssignment();
    CardinalPoint sideAssignment();
    PipeType pipeTypeAssignment();
    Bonus bonusAssignment();
    bool boolAssignment();
    BackgroundType backgroundAssignment();

    void ee();
    void pipe();
    void level();
    void printError(Str * s);

    int token;
    int level_number;

    bool error;
    int error_line;

    bool entry_ready;
    bool exit_ready;

    int start_delay;
    int restrict_coef;
    int fixed_coef;
    int speed;
    int required;
    BackgroundType background;

    List * graph_dirs;

    struct {
      int row;
      int column;
      PipeType type;
      CardinalPoint restricted_output1;
      CardinalPoint restricted_output2;
      Bonus bonus;
      bool fixed;
    }pipetable[BoardRows*BoardColumns]; //Full Board
    int npipes;
};

#endif

