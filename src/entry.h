/***************************************************************************
                          entry.h  -  description
                             -------------------
    begin                : Wed Jan 10 1996
    copyright            : (C) 1996 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ENTRY_H
#define ENTRY_H

#include "pipe.h"

class Entry: public Pipe{

  public:

    Entry(CardinalPoint connection);
    ~Entry();

    bool isRemovable();

    bool hasConnection(CardinalPoint con);

    CardinalPoint getOutput(CardinalPoint input);

    void restrictAsOutput(CardinalPoint con){};

    bool isRestrictedAsOutput(CardinalPoint con){return true;};

    void incFullLevel(CardinalPoint input,unsigned int amount);

    int getFullLevel(CardinalPoint input);

    Pointer * getPointer();

    void paint(VideoManager * vm);

    protected:

    CardinalPoint con;
};

#endif

