/***************************************************************************
                          pointerhorizontal.h  -  description
                             -------------------
    begin                : Fri Sep 15 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POINTER_HORIZONTAL_H
#define POINTER_HORIZONTAL_H

#include "pointer.h"

class PointerHorizontal: public Pointer{

  public:

  PointerHorizontal():Pointer(){
    image_name=new Str("phorizontal.png");
  }
};
#endif

