/***************************************************************************
                          playeronestream.h  -  description
                             -------------------
    begin                : Sat Sep 30 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PLAYER_ONE_STREAM_H
#define PLAYER_ONE_STREAM_H

#include "eventstream.h"
#include "SDL.h"

class PlayerOneStream : public EventStream{

  public:

    PlayerOneStream(){
      emt[0]=(struct EMT){SDLK_UP, UP};
      emt[1]=(struct EMT){SDLK_DOWN, DOWN};
      emt[2]=(struct EMT){SDLK_LEFT, LEFT};
      emt[3]=(struct EMT){SDLK_RIGHT, RIGHT};
      emt[4]=(struct EMT){SDLK_SPACE, HIT};
      emt[5]=(struct EMT){SDLK_ESCAPE, BACK};
      emt[6]=(struct EMT){SDLK_RETURN, GO};
      npairs=7;
    }
};
#endif

