/***************************************************************************
                          str.cpp  -  description
                             -------------------
    begin                : Tue Aug 1 2000
    copyright            : (C) 2000 by W. Baraldi & D. Scarpa
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

#include "str.h"
#include <string.h>

const char Str::nul = '\0';

Str::Str(const char * string=NULL){
  s=NULL;
  set(string);
}

Str::Str(Str * str){
  s=NULL;
  if (str) set(str->s);
}

Str::~Str(){
  if (s) delete[] s;
}

void Str::set(const char * string=NULL){
  if (s) delete[] s;
  if (string){
    s=new char[strlen(string)+1];
    strcpy(s, string);
  }else  s=NULL;
}

void Str::set(const Str * str){
  set(((Str*)(str))->get());
}

const char * Str::get(){
  if (s)
    return (s);
  else
    return (&nul);
}

bool Str::contains(const char * string){
  return (strstr(s, string));
}

bool Str::contains(Str * str){
  return (strstr(s, str->s));
}

void Str::concat(const char * string){
  char * aux=NULL;
  if (string){
    aux=new char[strlen(s)+strlen(string)+1];
    strcpy(aux, s),
    strcat(aux, string);
    free(s);
  }
  s=aux;
}

void Str::concat(Str * str){
  concat(str->s);
}

int Str::lenght(){
  return (strlen(s));
}

bool Str::isEqual(Str * str, bool case_sensitive = true){
  if (case_sensitive){
    return (!strcmp(s, str->s));
  }
  return (!strcasecmp(s, str->s));
}

void Str::crop(unsigned int start, unsigned int end){
  if (end>strlen(s)-1) end=strlen(s)-1;
  if (start<end){
    char * aux=new char[strlen(s)+1];
    char * aux2=s+start;
    strncpy(aux, aux2, end-start);
    aux[end-start]='\0';
    delete[] s;
    s=aux;
  }else
    set();
}

int Str::toInt(){
  return atoi(s);
}

bool Str::toBool(){
  if (!strcasecmp("f", s)) return (false);
  if (!strcasecmp("false", s)) return (false);
  if (!strcasecmp("t", s)) return (true);
  if (!strcasecmp("true", s)) return (true);
  return false;
}

float Str::toFloat(){
  return (atof(s));
}

