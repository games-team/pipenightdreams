/***************************************************************************
                          pointerhorizontalbowl.h  -  description
                             -------------------
    begin                : Mon Jan 1 1996
    copyright            : (C) 1996 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POINTER_HORIZONTAL_BOWL_H
#define POINTER_HORIZONTAL_BOWL_H

#include "pointer.h"

class PointerHorizontalBowl: public Pointer{

  public:
    PointerHorizontalBowl():Pointer(){
      image_name=new Str("pbowl_h.png");
    }
};


#endif

