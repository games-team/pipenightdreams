/***************************************************************************
                          image.cpp  -  description
                             -------------------
    begin                : Sat Aug 19 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "image.h"
#include "SDL_image.h"
#include <stdio.h>

Image::Image(Str * filename=NULL):Graphic(){
  if (filename) load(filename);
}

Image::Image(Image * im):Graphic(im){}

void Image::load(Str * filename){
  SDL_Surface * aux_surface;

  IMG_InvertAlpha(1);
  if (surface) SDL_FreeSurface(surface);
  aux_surface=IMG_Load(filename->get());
  if (!aux_surface){
    printf("Fatal Error :  File %s not found\n", filename->get());
    exit(-1);
  }
  surface=SDL_DisplayFormatAlpha(aux_surface);
	SDL_FreeSurface(aux_surface);
  delete filename;
}

