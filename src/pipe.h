/***************************************************************************
                          pipe.h  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PIPE_H
#define PIPE_H

#include "animatedcanvas.h"
#include "pointer.h"
#include "videomanager.h"
#include "sprite.h"

#define NORMAL_BONUS_VALUE	5
#define SUPER_BONUS_VALUE	10
#define ULTRA_BONUS_VALUE	25
#define HYPER_BONUS_VALUE	50

enum CardinalPoint {Void, South, West, North, East};
enum Bonus {NormalBonus, SuperBonus, UltraBonus,
            HyperBonus, LifeBonus, TimeBonus};

static const int PipeWidth=60;
static const int PipeHeight=60;

class Player;//Can understan this fucking need

class Pipe: public AnimatedCanvas{

  public:

    /** Constructor default. */
    Pipe();

    /** Destructor. */
    virtual ~Pipe();

    int width(){return PipeWidth;}
    int height(){return PipeHeight;}
    virtual int full(){return DefaultCapacity;}

    /** Retorna true si existe una conexi�n en ese punto cardinal. */
    virtual bool hasConnection(CardinalPoint con)=0;

    /** Retorna true si el pipe puede ser reemplazado por otro.*/
    virtual bool isRemovable();

    /** Setea la capacidad de ser removido de un pipe*/
    virtual void setFixed(bool flag=true);

    /** Setea el tipo de bonus*/
    virtual void setBonus(Bonus bonus=NormalBonus);

    /** Retorna el tipo de bonus*/
    virtual Bonus getBonus();

    /** Setea el owner del pipe*/
    virtual void setOwner(Player * owner);

    /** Retorna el owner del pipe*/
    virtual Player * getOwner();

    /** Restringe la conexi�n como salida.
        No hace nada si la conexi�n no existe.*/
    virtual void restrictAsOutput(CardinalPoint con)=0;

    /** Retorna true si la conexi�n esta restruingida como salida.*/
    virtual bool isRestrictedAsOutput(CardinalPoint con)=0;

    /** Retorna la salida de ese punto cardinal. El resultado es Void
       si no existe el input.*/
    virtual CardinalPoint getOutput(CardinalPoint input)=0;

    virtual bool hasLiquid();


    virtual void setLast(bool flag);

    virtual bool isLast();

    /** Incrementa el nivel de llenado en 1 para la conexi�n con ese input.
        El resultado es 0 si no existe input.*/
    virtual void incFullLevel(CardinalPoint input, unsigned int amount)=0;

      /** Retorna el nivel de llenado para ese input. Si el input no existe
    retorna -1.*/
    virtual int getFullLevel(CardinalPoint input)=0;

    /** Retorna una referencia al puntero asociado.*/
    virtual Pointer * getPointer()=0;

    /** Para uso futuro. Cambiarla a abstracta e
    implementarla en cada pipe*/
    void tick();

    protected:

      static const int Gran=50;
      static const int DefaultCapacity=1000;

      virtual void paintRestriction(VideoManager * vm, CardinalPoint con);
      virtual void paintBonus(VideoManager * vm, Bonus bonus);
      virtual void paintExclamation(VideoManager * vm);

      int full_level;
      CardinalPoint used_input;
      Pointer * p;
      CardinalPoint ro;
      bool fixed;
      Bonus bonus;
      Player * owner;
      Sprite * excl;
      int exclFrame;
      bool last;

      int bonusFrame;
      Sprite * bonusSprite;
};
#endif


