/***************************************************************************
                          verticalbowl.cpp  -  description
                             -------------------
    begin                : Thu Dec 28 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "verticalbowl.h"
#include "pointerverticalbowl.h"

VerticalBowl::VerticalBowl():Vertical(){
  delete p; /* Esto es asqueroso pero no encontr� otra soluci�n para
  llamar solo a Pipe()*/
  p=new PointerVerticalBowl();
  bowl_level=0;
}

void VerticalBowl::incFullLevel(CardinalPoint input,unsigned int amount){
  if ((input==North || input==South) && input!=ro)
    if ((used_input==Void) || (used_input==input)){
      if (bowl_level>=BowlCapacity)
        full_level+=amount;
      else
        if (full_level>DefaultCapacity/2)
          bowl_level+=amount;
        else
          full_level+=amount;
      used_input=input;
    }
}

void VerticalBowl::paint(VideoManager * vm){

  Image * ima;
  if (fixed)
    ima=(vm->getImageManager())->getImage(new Str("bowl_vb.png"));
  else
    ima=(vm->getImageManager())->getImage(new Str("bowl_v.png"));
  vm->blit(ima, x, y);

  if (ro!=Void) paintRestriction(vm, ro);

  paintFlow(vm);

  if (bowl_level>BowlCapacity/2){
    ima=(vm->getImageManager())->getImage(new Str("flow_big.png"));
    vm->blit(ima, x, y);
  }
  if (bonus!=NormalBonus) paintBonus(vm, bonus);
}


