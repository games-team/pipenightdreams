/***************************************************************************
                          score.cpp  -  description
                             -------------------
    begin                : Sun Mar 11 2001
    copyright            : (C) 2001 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "score.h"
#include <math.h>

Score::Score(int value=0){
  this->value=value;
  delta=0;
  changed=true;
}

Score::~Score(){}

void Score::inc(unsigned int value){
  changed=true;
  delta+=value;
}

void Score::dec(unsigned int value){
  changed=true;
  delta-=value;
}

int Score::getValue(){
  return value+delta;
}

bool Score::isChanged(){
  return changed;
}

void Score::sumUp(){
  if (delta > 0)
    value+=delta;
  else
    if (delta<0)
      if ((unsigned int)abs(delta)<value)
        value-=(unsigned int)abs(delta);
  delta=0;
}

void Score::paint(VideoManager * vm){
  // Borra
  vm->fillRect(x,y,ScoreWidth,ScoreHeight,0,0,0,255);
  // Dibuja los numeros
  int p=x+ScoreWidth-2;
  unsigned int i;
  int aux_value=value;
  Image * ima;
  for (i=0; i<Digits;i++){
    switch (aux_value%10){
      case 0:{
        ima=(vm->getImageManager())->getImage(new Str("0.png"));
        break;
      }
      case 1:{
        ima=(vm->getImageManager())->getImage(new Str("1.png"));
        break;
      }
      case 2:{
        ima=(vm->getImageManager())->getImage(new Str("2.png"));
        break;
      }
      case 3:{
        ima=(vm->getImageManager())->getImage(new Str("3.png"));
        break;
      }
      case 4:{
        ima=(vm->getImageManager())->getImage(new Str("4.png"));
        break;
      }
      case 5:{
        ima=(vm->getImageManager())->getImage(new Str("5.png"));
        break;
      }
      case 6:{
        ima=(vm->getImageManager())->getImage(new Str("6.png"));
        break;
      }
      case 7:{
        ima=(vm->getImageManager())->getImage(new Str("7.png"));
        break;
      }
      case 8:{
        ima=(vm->getImageManager())->getImage(new Str("8.png"));
        break;
      }
      case 9:{
        ima=(vm->getImageManager())->getImage(new Str("9.png"));
        break;
      }
      default:break;
    }
    p-=ima->width();
    vm->blit(ima,p,y+2);
    aux_value/=10;
  }
  changed=false;
}

void Score::tick(){
  changed=true;
  if ((unsigned int)abs(delta) > TickValue){
    if (delta > 0){
      delta-=TickValue;
      value+=TickValue;
    }else{
      delta+=TickValue;
      if (value - TickValue > 0)
        value-=TickValue;
      else
        value=0;
    }
  }else{
    value+=delta;
    delta=0;
  }
}

