/***************************************************************************
                          exit.h  -  description
                             -------------------
    begin                : Sat Oct 21 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EXIT_H
#define EXIT_H

#include "pipe.h"

class Exit: public Pipe{

  public:

    Exit(CardinalPoint connection);
    ~Exit();

    bool isRemovable();

    bool hasConnection(CardinalPoint con);

    CardinalPoint getOutput(CardinalPoint input);

    void restrictAsOutput(CardinalPoint con){};

    bool isRestrictedAsOutput(CardinalPoint con){return false;};

    void incFullLevel(CardinalPoint input,unsigned int amount);

    int getFullLevel(CardinalPoint input);

    Pointer * getPointer();

    void paint(VideoManager * vm);

    protected:

    CardinalPoint con;
};

#endif

