/***************************************************************************
                          paper.h  -  description
                             -------------------
    begin                : Fri Jan 18 2002
    copyright            : (C) 2002 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PAPER_H
#define PAPER_H

#include "animatedcanvas.h"

class Sprite;
class ImageManager;

class Paper: public AnimatedCanvas{

  public:
    static const int DeltaY;
    static const int NFrames;

    enum PaperColor {Red, Green, Blue};

    Paper(int maxY, PaperColor color, ImageManager * im);
    virtual ~Paper();

    int width();

    int height();

    void paint(VideoManager * vm);

    void tick();

  protected:

	  Sprite * sprite;
	
	  int frame;
	
	  int maxY;
	
	  int l_width;
	
	  int l_height;
};

#endif

