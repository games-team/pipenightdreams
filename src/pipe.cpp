/***************************************************************************
                          pipe.cpp  -  description
                             -------------------
    begin                : Wed Jan 10 1996
    copyright            : (C) 1996 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "pipe.h"

Pipe::Pipe():AnimatedCanvas(){
  full_level=0;
  used_input=Void;
  ro=Void;
  fixed=false;
  bonus=NormalBonus;
  this->owner=NULL;
  excl=NULL;
  exclFrame=0;
  last=false;
  bonusFrame=0;
  bonusSprite=NULL;
}

Pipe::~Pipe(){
  if (excl) delete excl;
}

bool Pipe::hasLiquid(){
  return (full_level>0);
}

bool Pipe::isRemovable(){
  return !(full_level>0) && !fixed;
}

void Pipe::setFixed(bool flag=true){
  fixed=flag;
}

void Pipe::setBonus(Bonus bonus=NormalBonus){
  this->bonus=bonus;
}

Bonus Pipe::getBonus(){
  return bonus;
}

void Pipe::setOwner(Player * owner){
  this->owner=owner;
}

Player * Pipe::getOwner(){
  return owner;
}

void Pipe::tick(){
  exclFrame = (exclFrame + 1) % 15;
  bonusFrame = (bonusFrame +1) % 30;
}

void Pipe::paintRestriction(VideoManager * vm, CardinalPoint con){
  Image * ima;
  switch (con){
    case West:{
      ima=(vm->getImageManager())->getImage(new Str("arrow_w.png"));
      vm->blit(ima, x, y);
      break;
    }
    case South:{
      ima=(vm->getImageManager())->getImage(new Str("arrow_s.png"));
      vm->blit(ima, x, y);
      break;
    }
    case East:{
      ima=(vm->getImageManager())->getImage(new Str("arrow_e.png"));
      vm->blit(ima, x, y);
      break;
    }
    case North:{
      ima=(vm->getImageManager())->getImage(new Str("arrow_n.png"));
      vm->blit(ima, x, y);
      break;
    }
    default:break;
  }
}

void Pipe::paintBonus(VideoManager * vm, Bonus bonus){
  Image * ima;
  if (!bonusSprite){
    switch (bonus){
      case SuperBonus:{
        bonusSprite= new Sprite(new Str("super_bonus"), new Str(".png"), 30, vm->getImageManager());
        break;
      }
      case UltraBonus:{
        bonusSprite= new Sprite(new Str("ultra_bonus"), new Str(".png"), 30, vm->getImageManager());
        break;
      }
      case HyperBonus:{
        bonusSprite= new Sprite(new Str("hyper_bonus"), new Str(".png"), 30, vm->getImageManager());
        break;
      }
      case LifeBonus:{
        bonusSprite= new Sprite(new Str("life_bonus"), new Str(".png"), 30, vm->getImageManager());
        break;
      }
      default:break;
    }
  }
  if (hasLiquid()>0)vm->blit(bonusSprite->frame(vm->getImageManager(), 0), x+(width()-bonusSprite->frameWidth(vm->getImageManager()))/2, y+(height()-bonusSprite->frameHeight(vm->getImageManager()))/2);
  else vm->blit(bonusSprite->frame(vm->getImageManager(), bonusFrame), x+(width()-bonusSprite->frameWidth(vm->getImageManager()))/2, y+(height()-bonusSprite->frameHeight(vm->getImageManager()))/2);
}

void Pipe::paintExclamation(VideoManager * vm){
  if (excl==NULL) excl=new Sprite(new Str("exclam"), new Str(".png"), 15, vm->getImageManager());
  vm->blit(excl->frame(vm->getImageManager(), exclFrame), x + width()-excl->frameWidth(vm->getImageManager()), y);
}

void Pipe::setLast(bool flag){
  last=flag;
}

bool Pipe::isLast(){
  return last;
}
