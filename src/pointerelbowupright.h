/***************************************************************************
                          pointerelbowupright.h  -  description
                             -------------------
    begin                : Fri Sep 15 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POINTER_ELBOW_UP_RIGHT_H
#define POINTER_ELBOW_UP_RIGHT_H

#include "pointer.h"

class PointerElbowUpRight: public Pointer{

  public:

  PointerElbowUpRight():Pointer(){
    image_name=new Str("pelbow_ne.png");
  }
};
#endif

