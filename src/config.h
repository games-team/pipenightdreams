/* pipenightdreams/config.h.  Generated automatically by configure.  */
/* pipenightdreams/config.h.in.  Generated automatically from configure.in by autoheader.  */

/* Define if lex declares yytext as a char * by default, not a char[].  */
#define YYTEXT_POINTER 1

/* #undef GAME_PREFIX */
#define GAME_DATADIR "/usr/local/share/games/pipenightdreams"

/* Define if you have the SDL_image library (-lSDL_image).  */
#define HAVE_LIBSDL_IMAGE 1

/* Name of package */
#define PACKAGE "pipenightdreams"

/* Version number of package */
#define VERSION "0.9.0"

