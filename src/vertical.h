/***************************************************************************
                          vertical.h  -  description
                             -------------------
    begin                : Thu Aug 17 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef VERTICAL_H
#define VERTICAL_H

#include "pipe.h"

class Vertical: public Pipe{

  public:

    Vertical();
    ~Vertical();

    bool hasConnection(CardinalPoint con);

    CardinalPoint getOutput(CardinalPoint input);

    void restrictAsOutput(CardinalPoint con);

    bool isRestrictedAsOutput(CardinalPoint con);

    void incFullLevel(CardinalPoint input,unsigned int amount);

    int getFullLevel(CardinalPoint input);

    Pointer * getPointer();

    void paint(VideoManager * vm);

  protected:

    void paintFlow(VideoManager * vm);

};

#endif

