/***************************************************************************
                          horizontalbowl.h  -  description
                             -------------------
    begin                : Mon Jan 1 1996
    copyright            : (C) 1996 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef HORIZONTAL_BOWL_H
#define HORIZONTAL_BOWL_H

#include "horizontal.h"

class HorizontalBowl: public Horizontal{

  public:

    HorizontalBowl();

    void incFullLevel(CardinalPoint input,unsigned int amount);

    void paint(VideoManager * vm);

  protected:

    static const int BowlCapacity=500;
    int bowl_level;
};

#endif

