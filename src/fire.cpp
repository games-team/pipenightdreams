/***************************************************************************
                          fire.cpp  -  description
                             -------------------
    begin                : Fri Jan 18 2002
    copyright            : (C) 2002 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fire.h"
#include "sprite.h"
#include "random.h"

const int Fire::NFrames=2;

Fire::Fire(ImageManager * im):AnimatedCanvas(){

  l_width=8;
  l_height=14;
  c=0;
  frame=0;

  sprite=new Sprite(new Str("fire"), new Str(".png"), NFrames, im);
}

Fire::~Fire(){
  delete sprite;
}

int Fire::width(){
  return l_width;
}

int Fire::height(){
 return l_height;
}


void Fire::paint(VideoManager * vm){
  vm->blit(sprite->frame(vm->getImageManager(), frame), x, y);
}

void Fire::tick(){
  if (c++%6==0) frame=(frame+1)%NFrames;
}
