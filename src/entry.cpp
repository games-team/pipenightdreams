/***************************************************************************
                          entry.cpp  -  description
                             -------------------
    begin                : Wed Jan 10 1996
    copyright            : (C) 1996 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "entry.h"

Entry::Entry(CardinalPoint connection):Pipe(){
  con=connection;
}

Entry::~Entry(){}

bool Entry::hasConnection(CardinalPoint con){
  return (this->con==con);
}

CardinalPoint Entry::getOutput(CardinalPoint input){
  return con;
}

void Entry::incFullLevel(CardinalPoint input,unsigned int amount){
  full_level+=amount;
  used_input=Void;
}

int Entry::getFullLevel(CardinalPoint input){
  return this->full_level;
}

Pointer * Entry::getPointer(){
  return NULL;
}

bool Entry::isRemovable(){
  return false;
}

void Entry::paint(VideoManager * vm){
  Image * aux;
  Image * ima;

  if (con==East || con==West)
    ima=(vm->getImageManager())->getImage(new Str("entry_h.png"));
  else
    ima=(vm->getImageManager())->getImage(new Str("entry_v.png"));
  aux=new Image(ima);
  switch (con){
    case South:{aux->flip(HAxis);aux->flip(VAxis);break;}
    case West:{aux->flip(VAxis);break;}
    default:break;
  }
  vm->blit(aux, x, y);
  delete aux;

  if (full_level>0){
    ima=(vm->getImageManager())->getImage(new Str("flow.png"));
    int xof=x+PipeWidth/2-ima->width()/2;
    int yof=y+PipeHeight/2-ima->height()/2;
    vm->setClipping(x, y, PipeWidth, PipeHeight);
    vm->enableClipping(true);
    switch (con){
      case North:case South:{
        float factor=0.5*(float)PipeHeight/(float)full();
        if (con==North) factor=-factor;
        for (int i=0;i<full_level+Gran;i+=Gran)
          vm->blit(ima, xof, (int)(yof+(float)i*factor));
        break;
      }
      case East: case West:{
        float factor=0.5*(float)PipeWidth/(float)full();
        if (con==West) factor=-factor;
        for (int i=0;i<full_level+Gran;i+=Gran)
          vm->blit(ima, (int)(xof+(float)i*factor), yof);
      }
      default:break;
    }
    vm->enableClipping(false);
  }
}

