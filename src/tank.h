/***************************************************************************
                          tank.h  -  description
                             -------------------
    begin                : Fri Dec 29 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TANK_H
#define TANK_H

#include "animatedcanvas.h"

class Tank: public AnimatedCanvas{

  public:

    /** Crea una tanque full_level lleno*/
    Tank(unsigned int full_level);

    /** Destructor*/
    ~Tank();

    /**Disminuye la cantidad en 1.*/
    void decFullLevel(unsigned int amount);

    /** Retorna true si el tanque esta vacio.*/
    bool isEmpty();

    void paint(VideoManager * vm);

    bool isChanged();

    void tick();

  protected:

    static const unsigned int Capacity=2000;
    static const unsigned int TankWidth=12;
    static const unsigned int TankHeight=180;

    int full_level;
    int state;
    bool changed;
};
#endif

