/***************************************************************************
                          pointer.cpp  -  description
                             -------------------
    begin                : Thu Sep 14 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "pointer.h"

Pointer::Pointer(int row=0, int column=0){
  this->row=row;
  this->column=column;
  this->moved_flag=true;
}

Pointer::~Pointer(){ delete image_name;}

void Pointer::setRow(int row){
  this->row=row;
}

void Pointer::setColumn(int column){
  this->column=column;
}

void Pointer::setRowColumn(int row, int column){
  this->row=row;
  this->column=column;
}

void Pointer::setMoved(bool flag){
  moved_flag=flag;
}

bool Pointer::moved(){
 return moved_flag;
}

int Pointer::getRow(){
  return row;
}

int Pointer::getColumn(){
  return column;
}

void Pointer::paint(VideoManager * vm){
  ima=((ImageManager *)(vm->getImageManager()))->getImage(new Str(image_name));
  vm->blit(ima, x, y);
}


