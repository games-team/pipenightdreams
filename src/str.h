/***************************************************************************
                          str.h  -  description
                             -------------------
    begin                : Tue Aug 1 2000
    copyright            : (C) 2000 by W. Baraldi & D. Scarpa
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/**
     This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
*/

#ifndef STR_H
#define STR_H

#include "object.h"
#include <stdlib.h>
#include <string.h>

class Str: public Object{

  public:

    /**
      Contructor default, asigna la cadena pasada como const char * .
      Para crear una cadena nula usar : new Str()
    */

    Str(const char * string=NULL);

    /**
      Contructor copy, crea una copia del Str pasado como par�metro
    */

    Str(Str * str);

    /**
      Destructor
    */
    ~Str();

    /**
      Asigna la cadena par�metro
    */
    void set(const char * string=NULL);

    /**
      Asigna la cadena par�metro
    */
    void set(const Str * str);

    /**
      Retorna la cadena como const char *.
      No debe alterarse la cadena obtenida ya que no es una copia.
    */
    const char * get();

    /**
      Concatena la cadena const char * enviada
    */
    void concat(const char * string);

    /**
      Concatena la cadena enviada
    */
    void concat(Str * str);

    /**
      Retorna true si la cadena const char * enviada esta contenida.
    */
    bool contains(const char * string);

    /**
      Retorna true si la cadena par�metro esta contenida.
    */
    bool contains(Str * str);

    /** Retorna la longitud de la cadena.*/
    int lenght();

    /** Compara con la cadena par�metro. Si case_sensitive es true entonces
        lo har� case sensitive.*/
    bool isEqual(Str * str, bool case_sensitive = true);

    /** Deja solo la parte contenida entre start y end.*/
    void crop(unsigned int start, unsigned int end);

    /**
      Retorna el int resultado de convertir la cadena.
    */
    int toInt();

    /**
      Retorna el bool resultado de convertir la cadena. Los valores reconocidos como
      false son : "f" o "false" y como true : "t" y "true", sin sensibilidad a may�sculas
      o min�sculas. El valor default devuelto es false;
    */
    bool toBool();

    /**
      Retorna el float resultado de convertir la cadena.
    */
    float toFloat();

  protected:
    char * s;
    static const char nul;
};

#endif


