/***************************************************************************
                          mosaic.h  -  description
                             -------------------
    begin                : Wed Mar 21 2001
    copyright            : (C) 2001 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MOSAIC_H
#define MOSAIC_H

#include "background.h"

class Mosaic: public Background{

  public:
    Mosaic(Image * image);
    ~Mosaic();

    void repaint(VideoManager * vm, int x, int y, int w, int h);
    void paint(VideoManager * vm);

  protected:

    Image * image;
};

#endif

