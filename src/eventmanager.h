/***************************************************************************
                          eventmanager.h  -  description
                             -------------------
    begin                : Fri Sep 15 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

#include "object.h"
#include "SDL.h"
#include "list.h"
#include "eventstream.h"

class EventManager: public Object{

  public:

    EventManager();
    ~EventManager();

    void addStream(EventStream * s);

    /** Los streams no son liberados ni al hacer
    remove ni en el destructor.*/
    void removeStream(EventStream * s);

    /** Pasa todos los eventos de los dispositivos
        a los streams. Si no hubo eventos en los dispositivos:
        si wait=true espera hasta que pueda pasar uno,
        si wait=false retorna.*/

    void pumpEvents(bool wait=false);

  protected:

    List * lista_streams;
};
#endif

