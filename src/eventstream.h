/***************************************************************************
                          eventstream.h  -  description
                             -------------------
    begin                : Sat Sep 30 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef EVENT_STREAM_H
#define EVENT_STREAM_H

#include "object.h"
#include "SDL.h"

enum Event {NONE, UP, DOWN, LEFT, RIGHT, HIT, GO, BACK, QUIT};

class EventStream : public Object{

  public:

    static const int MaxEvents=16;

    EventStream();
    ~EventStream();

    /** Retornará true si el evento pertenece a este stream y
        fue agregado, caso contrario retornará false.*/
    bool put(SDLKey e);

    /** Retorna el proximo evento en la cola, None si no hay ninguno.*/
    Event get();

    void clear();

  protected:

    static const int MaxPairs=16;

    struct EMT{
      SDLKey sdlKeySym;
      Event event;
    }emt[MaxPairs];

    int npairs;

    Event events[MaxEvents];
    int ul, dl;
};

#endif


