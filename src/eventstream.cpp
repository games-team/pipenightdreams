/***************************************************************************
                          eventstream.cpp  -  description
                             -------------------
    begin                : Sat Sep 30 2000
    copyright            : (C) 2000 by Waldemar Baraldi
    email                : baraldi@lacasilla.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "eventstream.h"

EventStream::EventStream():Object(){
  ul=dl=0;
  npairs=0;
}

EventStream::~EventStream(){}

bool EventStream::put(SDLKey e){
  int i;
  for (i=0;i<npairs;i++){
    if (emt[i].sdlKeySym==e){
      events[ul]=emt[i].event;
      ul=(ul + 1) % MaxEvents;
      return true;
    }
  }
  return false;
}

Event EventStream::get(){
  if (dl!=ul){
    int i=dl;
    dl=(dl + 1) % MaxEvents;
    return events[i];
  }
  return NONE;
}

void EventStream::clear(){
  ul=dl;
}

